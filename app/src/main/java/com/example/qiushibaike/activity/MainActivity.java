package com.example.qiushibaike.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import com.example.qiushibaike.R;
import com.example.qiushibaike.fragment.FunnyImageFragment;
import com.example.qiushibaike.fragment.JokeFragment;
import com.example.qiushibaike.fragment.TodayInHistoryFragment;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * 主界面
 */
public class MainActivity extends AppCompatActivity {

    @BindView(R.id.fl_container)
    FrameLayout mFlContainer;
    @BindView(R.id.ll_joke)
    LinearLayout mLlJoke;
    @BindView(R.id.ll_funnyimage)
    LinearLayout mLlFunnyimage;
    @BindView(R.id.ll_history)
    LinearLayout mLlHistory;
    @BindView(R.id.ll_me)
    LinearLayout mLlMe;

    private JokeFragment mJokeFragment;
    private FunnyImageFragment mImageFragment;
    private TodayInHistoryFragment mHistoryFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        mJokeFragment = new JokeFragment();
        getSupportFragmentManager().beginTransaction().replace(R.id.fl_container, mJokeFragment).commit();
    }

    @OnClick({R.id.ll_joke, R.id.ll_funnyimage, R.id.ll_history, R.id.ll_me})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ll_joke:
                getSupportFragmentManager().beginTransaction().replace(R.id.fl_container, mJokeFragment).commit();
                break;
            case R.id.ll_funnyimage:
                if (mImageFragment == null) {
                    mImageFragment = new FunnyImageFragment();
                }
                getSupportFragmentManager().beginTransaction().replace(R.id.fl_container, mImageFragment).commit();
                break;
            case R.id.ll_history:
                if (mHistoryFragment == null) {
                    mHistoryFragment = new TodayInHistoryFragment();
                }
                getSupportFragmentManager().beginTransaction().replace(R.id.fl_container, mHistoryFragment).commit();
                break;
            case R.id.ll_me:
                break;
        }
    }
}
