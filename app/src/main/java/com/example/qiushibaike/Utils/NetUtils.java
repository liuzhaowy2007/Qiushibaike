package com.example.qiushibaike.Utils;

import java.io.IOException;

import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class NetUtils {

    private static OkHttpClient sClient = new OkHttpClient();

    /**
     * Get请求
     *
     * @param url
     * @return
     * @throws IOException
     */
    public static String getData(String url) throws IOException {
        Request request = new Request.Builder()
                .url(url)
                .build();
        Response response = sClient.newCall(request).execute();
        return response.body().string();
    }

    /**
     * post请求
     *
     * @param url
     * @param body
     * @return
     * @throws IOException
     */
    public static String postData(String url, FormBody body) throws IOException {
        Request request = new Request.Builder()
                .url(url)
                .post(body)
                .build();
        Response response = sClient.newCall(request).execute();
        return response.body().string();
    }
}
