package com.example.qiushibaike.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.qiushibaike.model.JokeItem;
import com.example.qiushibaike.R;

import java.util.List;

public class JokeAdapter extends RecyclerView.Adapter<JokeAdapter.ViewHolder> {

    public List<JokeItem> mResult;

    public JokeAdapter(List<JokeItem> result) {
        this.mResult = result;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.jokeitemview, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        JokeItem jokeItem = mResult.get(position);
        if (TextUtils.isEmpty(jokeItem.url)) {
            holder.textView.setText(jokeItem.content);
        } else {
            holder.textView.setVisibility(View.GONE);
            Glide.with(holder.imageView.getContext()).load(jokeItem.url).into(holder.imageView);
        }
    }

    @Override
    public int getItemCount() {
        return mResult == null ? 0 : mResult.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        TextView textView;
        ImageView imageView;

        public ViewHolder(View view) {
            super(view);
            textView = view.findViewById(R.id.tv_jokeitem);
            imageView = view.findViewById(R.id.iv_jokeitem);
        }
    }

}
