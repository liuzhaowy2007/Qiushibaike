package com.example.qiushibaike.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.qiushibaike.model.HistoryItem;
import com.example.qiushibaike.R;

import java.util.List;

public class HistoryAdapter extends RecyclerView.Adapter<HistoryAdapter.ViewHolder> {

    public List<HistoryItem> mResult;

    public HistoryAdapter(List<HistoryItem> result) {
        this.mResult = result;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.historyitemview, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        HistoryItem jokeItem = mResult.get(position);
        holder.tv_title.setText(jokeItem.title);
        holder.tv_date.setText(jokeItem.date);
    }

    @Override
    public int getItemCount() {
        return mResult == null ? 0 : mResult.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        TextView tv_title;
        TextView tv_date;

        public ViewHolder(View view) {
            super(view);
            tv_title = view.findViewById(R.id.tv_historytitle);
            tv_date = view.findViewById(R.id.tv_historydate);
        }
    }

}
