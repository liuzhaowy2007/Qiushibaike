package com.example.qiushibaike.fragment;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.alibaba.fastjson.JSON;
import com.example.qiushibaike.adapter.HistoryAdapter;
import com.example.qiushibaike.model.HistoryModel;
import com.example.qiushibaike.Utils.NetUtils;
import com.example.qiushibaike.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Calendar;
import java.util.concurrent.Executors;

import okhttp3.FormBody;

/**
 * 历史上的今天模块
 */
public class TodayInHistoryFragment extends Fragment {

    public static String URL = "http://v.juhe.cn/todayOnhistory/queryEvent.php";
    public static String KEY_HISTORY = "f5f7d655ef148f6bb777c80167f7f6de";

    private RecyclerView mRecyclerView;
    private HistoryAdapter mAdapter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Calendar calendar = Calendar.getInstance();

        final int month = calendar.get(Calendar.MONTH) + 1;
        final int day = calendar.get(Calendar.DAY_OF_MONTH);

        Executors.newSingleThreadExecutor().execute(new Runnable() {
            @Override
            public void run() {
                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put("date", month + "/" + day);
                    jsonObject.put("key", KEY_HISTORY);


                    FormBody body = new FormBody.Builder()
                            .add("date", month + "/" + day)
                            .add("key", KEY_HISTORY)
                            .build();

                    String result = NetUtils.postData(URL, body);
                    final HistoryModel model = JSON.parseObject(result, HistoryModel.class);
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            LinearLayoutManager layoutmanager = new LinearLayoutManager(getActivity());
                            //设置RecyclerView 布局
                            mRecyclerView.setLayoutManager(layoutmanager);
                            mAdapter = new HistoryAdapter(model.result);
                            mRecyclerView.setAdapter(mAdapter);
                        }
                    });
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = View.inflate(getActivity(), R.layout.fragment_joke, null);
        mRecyclerView = view.findViewById(R.id.recycleview);
        return view;
    }


}
