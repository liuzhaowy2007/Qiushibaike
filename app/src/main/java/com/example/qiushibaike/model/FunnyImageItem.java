/**
  * Copyright 2018 bejson.com 
  */
package com.example.qiushibaike.model;

/**
 * Auto-generated: 2018-10-19 9:44:26
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class FunnyImageItem {

    public String content;
    public String hashId;
    public long unixtime;
    public String url;

}