/**
 * Copyright 2018 bejson.com
 */
package com.example.qiushibaike.model;

import java.util.List;

public class JokeModel {

    public String reason;
    public List<JokeItem> result;
    public int error_code;

}